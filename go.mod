module codeberg.org/BubbyRoosh/stockfinder

go 1.17

require (
	github.com/piquette/finance-go v1.0.0
	github.com/sirsean/go-pool v0.0.0-20170808185629-2b94e61c3882
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c
)

require (
	github.com/shopspring/decimal v1.3.1 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
)
