package main

import (
	"io/ioutil"
	"log"
	"math"

	"gopkg.in/yaml.v3"
)

type Volume struct {
	Enabled bool `yaml:"enabled"`
	Min     int  `yaml:"min"`
	Max     int  `yaml:"max"`
}

func DefaultVolume() Volume {
	return Volume{
		Min: 0,
		Max: math.MaxInt,
	}
}

type Doji struct {
	Enabled bool    `yaml:"enabled"`
	Percent float64 `yaml:"percent"`
}

func DefaultDoji() Doji {
	return Doji{
		// FIXME: doesn't seem to be working :\
		Percent: .001,
	}
}

type Kicker struct {
	Enabled bool    `yaml:"enabled"`
	MinDoji float64 `yaml:"mindoji"`
}

func DefaultKicker() Kicker {
	return Kicker{
		MinDoji: .01,
	}
}

type Config struct {
	Volume Volume `yaml:"volume"`
	Doji   Doji   `yaml:"doji"`
	Kicker Kicker `yaml:"kicker"`
}

func DefaultConfig() Config {
	return Config{
		Volume: DefaultVolume(),
		Doji:   DefaultDoji(),
		Kicker: DefaultKicker(),
	}
}

func ReadConfig(path string) Config {
	config := DefaultConfig()
	data, err := ioutil.ReadFile(path)
	if err == nil {
		err := yaml.Unmarshal(data, &config)
		if err != nil {
			log.Fatal(err)
		}
	}
	return config
}
