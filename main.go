package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
	"time"

	"github.com/piquette/finance-go"
	"github.com/piquette/finance-go/chart"
	"github.com/piquette/finance-go/datetime"
	"github.com/sirsean/go-pool"
)

const StocksFileName = "stocks"

type Work struct {
	symbol  string
	filters []Filter
}

func (w Work) Perform() {
	maxBack := 1
	for _, filter := range w.filters {
		newBack := filter.Range()
		if newBack > maxBack {
			maxBack = newBack
		}
	}

	now := time.Now().UTC()
	if now.Weekday() == time.Saturday {
		now = now.AddDate(0, 0, -1)
	} else if now.Weekday() == time.Sunday {
		now = now.AddDate(0, 0, -2)
	}

	lenBack := now.AddDate(0, 0, maxBack*-1)
	params := &chart.Params{
		Symbol:   w.symbol,
		Start:    datetime.New(&lenBack),
		End:      datetime.New(&now),
		Interval: datetime.OneDay,
	}

	quotes := chart.Get(params)

	if err := quotes.Err(); err != nil {
		log.Println(err)
	}

	bars := make([]*finance.ChartBar, 0)
	for quotes.Next() {
		q := quotes.Bar()
		bars = append(bars, q)
	}
	if len(bars) < maxBack {
		return
	}
	for _, filter := range w.filters {
		if !filter.Apply(bars) {
			return
		}
	}
	fmt.Println(w.symbol)
}

func main() {
	config := ReadConfig("config.yaml")

	filters := MakeFilters(config)

	data, err := ioutil.ReadFile(StocksFileName)
	if err != nil {
		log.Fatal(err)
	}

	stocks := strings.Split(strings.ReplaceAll(string(data), "\r", ""), "\n")

	max := 100
	pool := pool.NewPool(max, max*2)
	pool.Start()
	for _, symbol := range stocks {
		pool.Add(Work{symbol, filters})
	}
	pool.Close()
}
