package main

import (
	"github.com/piquette/finance-go"
)

func IsDoji(bar *finance.ChartBar, maxPercent float64) bool {
	openNum := bar.Open.InexactFloat64()
	closeNum := bar.Close.InexactFloat64()
	highNum := bar.High.InexactFloat64()
	lowNum := bar.Low.InexactFloat64()

	var openDifference float64
	if openNum > closeNum {
		openDifference = 1 - closeNum/openNum
	} else {
		openDifference = 1 - openNum/closeNum
	}

	var highDifference float64
	if highNum > lowNum {
		highDifference = 1 - lowNum/highNum
	} else {
		highDifference = 1 - highNum/lowNum
	}
	return highDifference-openDifference <= maxPercent
}

type Filter interface {
	Range() int
	Apply([]*finance.ChartBar) bool
}

func (Volume) Range() int { return 1 }
func (v Volume) Apply(i []*finance.ChartBar) bool {
	q := i[0]
	return q.Volume > v.Min && q.Volume < v.Max
}

func (Doji) Range() int { return 1 }
func (d Doji) Apply(i []*finance.ChartBar) bool {
	q := i[0]
	// No this isn't 100% accurate to the 59249th place, but it's *good enough*
	return IsDoji(q, d.Percent)
}

func (Kicker) Range() int { return 2 }
func (k Kicker) Apply(i []*finance.ChartBar) bool {
	// FIXME: Not sure of day order
	yesterday := i[0]
	today := i[1]
	if yesterday.Open.GreaterThan(yesterday.Close) &&
		today.Open.GreaterThan(yesterday.Open) &&
		today.Close.GreaterThan(yesterday.Open) &&
		today.Close.GreaterThan(today.Open) {
		return !IsDoji(yesterday, k.MinDoji)
	}
	return false
}

func MakeFilters(config Config) []Filter {
	filters := make([]Filter, 0)
	if config.Volume.Enabled {
		filters = append(filters, config.Volume)
	}
	if config.Doji.Enabled {
		filters = append(filters, config.Doji)
	}
	if config.Kicker.Enabled {
		filters = append(filters, config.Kicker)
	}
	return filters
}
